# Hey ! :wave: Je suis Astrid 
[![Twitch Status](https://img.shields.io/twitch/status/teloru?style=flat&label=Twitch&logo=twitch)](https://www.twitch.tv/teloru)
<img align="right" src="https://gitlab.com/Astrid-Beyer/Astrid-Beyer/-/raw/main/icon.png" width="200">
<br>
<br>

- :mortar_board: CS Master's Degree, Computer Graphics & Applied Geometry
- :round_pushpin: Marseille, France
- :blue_heart: ThreeJS, C++, UI/UX, real-time graphics, VR enthusiast
- :movie_camera: Streaming on Twitch
